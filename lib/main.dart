import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:jsonExample/current_weather_small_card.dart';
import 'package:http/http.dart';
import 'current_weather_big_card.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;
  bool _expand = false;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  bool _expand = false;
  bool _loading = true;
  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  double temp = 1;

  void _expandPressed() {
    print("_expandPressed");
    setState(() {
      _expand = true;
    });
  }

  @override
  void initState() {
    super.initState();
    _onInit();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: AppBar(
        //   title: Text(widget.title),
        // ),
        body: Stack(
      fit: StackFit.expand,
      children: [
        _bodyWidget(context),
        _loading
            ? Container(
          alignment: Alignment.center,
              child: Container(
                  height: 35,
                  width: 35,
                  child: CircularProgressIndicator(
                    backgroundColor: Colors.red,
                  ),
                ),
            )
            : SizedBox()
      ],
    ));
  }

  Widget _bodyWidget(BuildContext context) {
    return SingleChildScrollView(
      physics: _expand
          ? AlwaysScrollableScrollPhysics()
          : NeverScrollableScrollPhysics(),
      child: Column(
        children: [
          SizedBox(
            height: 30,
          ),
          AnimatedContainer(
              duration: Duration(milliseconds: 300),
              child: _expand
                  ? CurrentWeatherSmallCard(temp: temp)
                  : CurrentWeatherCard(temp: temp)),
          SizedBox(
            height: 16,
          ),
          _listHourWeatherWidget(context),
          _expand ? SizedBox() : _foreastWidget(context),
          SizedBox(
            height: 16,
          ),
          _listforcastWidget(context),
        ],
      ),
    );
  }

  Widget _listHourWeatherWidget(BuildContext context) {
    return Container(
      height: 140,
      decoration: BoxDecoration(color: Color(0xff2C79C1)),
    );
  }

  _foreastWidget(BuildContext context) {
    return InkWell(
      onTap: _expandPressed,
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(16),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Forcats for 7 Days",
              style: TextStyle(
                  color: Color(0xff2C79C1),
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            ),
            Image.asset(
              "assets/eva_arrow-ios-downward-outline.png",
              width: 24,
              height: 24,
              color: Color(0xff2C79C1),
            )
          ],
        ),
      ),
    );
  }

  Widget _listforcastWidget(BuildContext context) {
    return Container(
      height: 379,
      width: double.infinity,
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: Color(0xff2C79C1),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Forcats for 7 Days",
            style: TextStyle(
                color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500),
          ),
        ],
      ),
    );
  }

  void _onInit() async {
    var client = Client();
    try {
      var response = await client.get(
          "https://api.openweathermap.org/data/2.5/weather?lat=21.031789&lon=105.8283798&appid=98fb39f227cd6adf68e00cc3529bf818&units=metric");
      var decodedResponse = jsonDecode(utf8.decode(response.bodyBytes)) as Map;

      final _temp = decodedResponse["wind"]["speed"] as double;
      if (decodedResponse != null) {
        setState(() {
          temp = _temp;
          _loading = false;
        });
      }
    } catch (error) {
      print(error);
    } finally {
      client.close();
    }
  }
}
