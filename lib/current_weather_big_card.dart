import 'package:flutter/material.dart';

class CurrentWeatherCard extends StatelessWidget {
  final double temp;
  const CurrentWeatherCard({Key key, this.temp}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 565,
      padding: EdgeInsets.all(16),
      margin: EdgeInsets.all(16),
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: [
            Color(0xff62B8F6),
            Color(0xff2C79C1),
          ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
          borderRadius: BorderRadius.circular(30)),
      child: Column(
        children: [
          _headerWidget(context),
          _tempWidget(context),
          Divider(
            height: 32,
            color: Colors.white,
          ),
          _bottomWidget(context),
        ],
      ),
    );
  }

  Widget _headerWidget(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            "assets/akar-icons_plus.png",
            width: 32,
            height: 32,
            color: Colors.white,
          ),
          _titleWidget(context),
          Image.asset(
            "assets/carbon_overflow-menu-vertical.png",
            width: 32,
            height: 32,
            color: Colors.white,
          ),
        ],
      ),
    );
  }

  _titleWidget(BuildContext context) {
    return Column(
      children: [
        Text(
          "Malang",
          style: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 16,
            letterSpacing: -0.24,
            color: Colors.white,
          ),
        ),
        SizedBox(
          height: 4,
        ),
        _indicatorsWidget(context),
      ],
    );
  }

  Widget _indicatorsWidget(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _indicatorWidget(context, value: true),
        SizedBox(
          width: 4,
        ),
        _indicatorWidget(context, value: false),
        SizedBox(
          width: 4,
        ),
        _indicatorWidget(context, value: false),
      ],
    );
  }

  Widget _indicatorWidget(BuildContext context, {bool value}) {
    bool indicator = value ?? false;
    return AnimatedContainer(
      width: 8,
      height: 8,
      decoration: BoxDecoration(
          color: indicator ? Colors.white : Colors.transparent,
          borderRadius: BorderRadius.circular(100),
          border: Border.all(color: Colors.white)),
      duration: Duration(milliseconds: 300),
    );
  }

  Widget _tempWidget(BuildContext context) {
    return Column(
      children: [
        Image.asset(
          "assets/sun.png",
          width: 240,
          height: 240,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Sunday",
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
            Container(
              width: 2,
              height: 19,
              color: Colors.white,
              margin: EdgeInsets.symmetric(horizontal: 11),
            ),
            Text(
              "Nov 14",
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
          ],
        ),
        SizedBox(
          height: 16,
        ),
        Container(
          child: Text(
            "${temp}°",
            style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 72,
              color: Colors.white,
            ),
          ),
        ),
        Container(
          child: Text(
            "Heavy rain",
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
            ),
          ),
        )
      ],
    );
  }

  Widget _bottomWidget(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Expanded(
            child: Column(
              children: [
                Row(
                  children: [
                    Image.asset(
                      "assets/carbon_location-current.png",
                      width: 32,
                      height: 32,
                      color: Colors.white,
                    ),
                    SizedBox(width: 4,),
                    Column(crossAxisAlignment: CrossAxisAlignment.start,children: [
                      Text("3.7 km/h", style: TextStyle(fontSize: 12, color: Colors.white),),
                      Text("Wind", style: TextStyle(fontSize: 12, color: Colors.white)),
                    ],)
                  ],
                ),
                SizedBox(height: 24,),
                Row(
                  children: [
                    Image.asset(
                      "assets/fluent_temperature-24-regular.png",
                      width: 32,
                      height: 32,
                      color: Colors.white,
                    ),
                    SizedBox(width: 4,),
                    Column(crossAxisAlignment: CrossAxisAlignment.start,children: [
                      Text("1010 mbar", style: TextStyle(fontSize: 12, color: Colors.white),),
                      Text("Pressure", style: TextStyle(fontSize: 12, color: Colors.white)),
                    ],)
                  ],
                )
              ],
            ),
          ),
          Expanded(
            child: Column(
              children: [
                Row(
                  children: [
                    Image.asset(
                      "assets/fluent_weather-rain-24-regular.png",
                      width: 32,
                      height: 32,
                      color: Colors.white,
                    ),
                    SizedBox(width: 4,),
                    Column(crossAxisAlignment: CrossAxisAlignment.start,children: [
                      Text("74%", style: TextStyle(fontSize: 12, color: Colors.white),),
                      Text("Chance of rain", style: TextStyle(fontSize: 12, color: Colors.white)),
                    ],)
                  ],
                ),
                SizedBox(height: 24,),
                Row(
                  children: [
                    Image.asset(
                      "assets/ion_water-outline.png",
                      width: 32,
                      height: 32,
                      color: Colors.white,
                    ),
                    SizedBox(width: 4,),
                    Column(crossAxisAlignment: CrossAxisAlignment.start,children: [
                      Text("83%", style: TextStyle(fontSize: 12, color: Colors.white),),
                      Text("Humidity 83%", style: TextStyle(fontSize: 12, color: Colors.white)),
                    ],)
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
